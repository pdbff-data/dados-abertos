# Dados de Localidade/Locality Data

Os dois arquivos são apenas dois  (kml e csv) formatos da mesma informação.

Na tabela cada linha é ou uma parcela ou uma subparcela (coluna name) e os valores únicos se dão pela combinação parent+name. As seguintes colunas:
   - reserva:  primeiro nível de localidade dentro da ARIE PDBFF. Não há um polígono para este nível.
   - parent:   Localidade pai da coluna *nome*. Equivale a Plot/Parcela quando o nome é SubPlot/Quadrante
   - name:     Nome da parcela ou subparcela, cujas dimensões e geometria estão explicitados nas demais colunas
   - startx:   Longitude do vértice SW de name
   - starty:   Latitude do vértice SW de name
   - startx.m: Se name for uma subparcela, posicao em metros de startx em relacao ao startx do parent dessa linha (parcela)
   - starty.m: Mesmo para dimensao Y
   - dimx.m:   Dimensão X de name
   - dimy.m:   Dimensão Y de name
   - notes:    Notas sobre o georeferenciamento
   - geom:     Geometria de name no formato WKT (Well-known text)
   - created by and date:  para monitoramento da versão

# English

Both locality files have the same information and give the hierarchical structure of the plots and subplots within the BDFFP reserves.

In the CSV file, each row is either a plot (parcela) or a subplot (quadrat). Includes the following columns (parent+name make unique values that match plot+subplot):
   - reserva:  First PDBFF locality level, refer to the different Reserves and camp sites within the ARIE PDBFF that make 
   - parent:   Parent locality of *name*. equal to Plot when name is a SubPlot
   - name:     Name of the plot or subplot to which dimensions and geometry refer
   - startx:   Longitude of SW corner of name
   - starty:   Latitude of SW corner of name
   - startx.m: If name is a subplot, position startx in meters from parent startx
   - starty.m: If name is a subplot, position starty in meters from parent starty
   - dimx.m:   Dimension X of name
   - dimy.m:   Dimension Y of name
   - notes:    Notes regarding georeferencing
   - geom:     Geometry of polygon in WKT
   - created by and date:  version tracking of file and geometry.
   
# Version Remarks

**2018-04-19** - Primeira versão. Preparado por A. Vicentini.
* A coordenadas geográficas dos vértices e os polígonos das parcelas e subparcelas do PDBFF foram calculadas a partir de coordenadas de GPS tiradas nos vértices de cada parcela e de algumas subparcelas por diferentes pessoas e equipamentos. Para manter as dimensões reais das parcelas as coordenadas foram ajustadas. Os polígonos gerados não foram checados novamente em campo.
* This is the first version. Geometry and coordinates for all subplots were calculated from the GPS coordinates of plot (and some subplots) vertices and therefore are approximate values only. To keep the proper plot and subplot dimensions adjustments were made and calculated coordinates were not validated in the field. 

   
   
   