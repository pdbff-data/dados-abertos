# Dados abertos

Repositório de dados de acesso aberto do Projeto Dinâmica Biológica de Fragmentos Florestais (PDBFF), do Instituto Nacional de Pesquisas da Amazônia.

* **reservas-parcelas** - mapa e coordenadas geográficas das parcelas e subparcelas de inventário florestal no PDBFF. 